# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Unreleased

## 22.10.6 - 2022-10-09
## Changes
- [API]: switch to pydantic for request validation

## 22.10.5 - 2022-10-09
## Changes
- [API|configuration]: convert ([/v1/configuration/logging/level/{level}](https://morph027.gitlab.io/python-signal-cli-rest-api/swagger/index.html#/Configuration/post~configuration_logging_v1.configuration_logging_v1_post)) from 22.10.4 into ([/v1/configuration](https://morph027.gitlab.io/python-signal-cli-rest-api/swagger/index.html#/Configuration/post~configuration_logging_v1.configuration_logging_v1_post))

## 22.10.4 - 2022-10-04
## Changes
- [API|configuration]: add ([/v1/configuration/logging/level/{level}](https://morph027.gitlab.io/python-signal-cli-rest-api/swagger/index.html#/Configuration/post~configuration_logging_v1.configuration_logging_v1_post))

## 22.10.3 - 2022-10-03
## Changes
- [API]: fix error handling

## 22.10.2 - 2022-10-03
## Changes
- [API|send]: better group detection for groups where account is not member of
- [API|about]: add signal-cli version string

## 22.10.1 - 2022-10-03
## Changes
- [API|profiles]: new endpoint for profiles ([/v1/profiles/{number}](https://morph027.gitlab.io/python-signal-cli-rest-api/swagger/index.html#/Profiles/put~update_profile_v1.update_profile_v1_put))

## 22.9.2 - 2022-09-30
## Changes
- [API|send]: better group detection (#1)
- [API|general]: add tracebacks to gateway logs
- [CI]: update pre-commit hooks

## 22.9.1 - 2022-09-29
## Changes
- bump sanic to 22.9.0
- bump sanic-ext to 22.9.0

## 22.8.1 - 2022-08-11
## Changes
- bump sanic to 22.6.2
- bump sanic-ext to 22.6.3
- bump jmespath to 1.0.1
- pin pyyaml to 6.0

## 22.6.2 - 2022-06-30
## Fixes
- fix pypi upload

## 22.6.1 - 2022-06-30
## Changes
- bump sanic to 22.6.0
- bump sanic-ext to 22.6.1

## 22.4.1 - 2022-04-05
## Changes
- bump sanic-ext to 22.3.0

## 22.3.1 - 2022-03-05
## Changes
- allow specifying json rpc connection via app config
- add signal-cli daemon to docs

## 22.2.2 - 2022-02-03
## Changes
- increase minimum python version to 3.8 (`importlib.metadata` missing in 3.7)

## 22.2.1 - 2022-02-02
## Changes
- include swagger assets (css, js) into build

## 22.1.2 - 2022-01-30
### Changes
- update naming scheme to match pypi project name

## 22.1.1 - 2022-01-30
### Added
- initial commit (port from [signal-cli-dbus-rest-api](https://gitlab.com/morph027/signal-cli-dbus-rest-api), replacing DBus w/ JSON RPC)
