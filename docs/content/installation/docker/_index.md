+++
title = "Docker"
+++

## Run container

{{< tabs groupId="run" >}}
{{% tab name="Docker" %}}
```bash
docker run -d \
  --name signal-cli \
  --publish 7583:7583 \
  --volume /some/local/dir/signal-cli-config:/var/lib/signal-cli \
  --tmpfs /tmp:exec \
  registry.gitlab.com/packaging/signal-cli/signal-cli-<variant>:latest \
  daemon --tcp 0.0.0.0:7583
docker run -d \
  --name python-signal-cli-rest-api \
  --env PYTHON_SIGNAL_CLI_REST_API_JSONRPC_HOST=signal-cli \
  --env PYTHON_SIGNAL_CLI_REST_API_HOST=0.0.0.0 \
  --publish 8080:8080 \
  --tmpfs /run \
  --tmpfs /tmp:exec \
  registry.gitlab.com/morph027/python-signal-cli-rest-api/python-signal-cli-rest-api:latest
```
`<variant>`: `native` or `jre`.

If you like to use `/v1/send` without specifying a number in your POST request, you'll need to add
`--env PYTHON_SIGNAL_CLI_REST_API_ACCOUNT=+XX`, where XX is the number registered with signal-cli.
{{% /tab %}}
{{% tab name="Docker Compose" %}}
```yaml
version: "3"
services:
  signal-cli:
    image: registry.gitlab.com/packaging/signal-cli/signal-cli-<variant>:latest
    command: daemon --tcp 0.0.0.0:7583
    ports:
      - "7583:7583"
    volumes:
      - "/some/local/dir/signal-cli-config:/var/lib/signal-cli"
    tmpfs:
      - "/tmp:exec"
  python-signal-cli-rest-api:
    image: registry.gitlab.com/morph027/python-signal-cli-rest-api/python-signal-cli-rest-api:latest
    environment:
      - PYTHON_SIGNAL_CLI_REST_API_JSONRPC_HOST=signal-cli
      - PYTHON_SIGNAL_CLI_REST_API_HOST=0.0.0.0
      # - PYTHON_SIGNAL_CLI_REST_API_ACCOUNT=+XX # If you like to use `/v1/send` without specifying a number in your POST request
    ports:
      - "8080:8080"
    tmpfs:
      - "/run"
      - "/tmp:exec"
```
`<variant>`: `native` or `jre`.
{{% /tab %}}
{{< /tabs >}}

## Configuration

See [Configuration]({{< ref "/configuration" >}} "Configuration")
