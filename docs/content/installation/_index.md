+++
title = "Installation"
weight = 1
+++

* [Docker]({{< ref "/installation/docker" >}} "Docker")
* [Standalone]({{< ref "/installation/standalone" >}} "Standalone")
