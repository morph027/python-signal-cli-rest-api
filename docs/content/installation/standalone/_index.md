+++
title = "Standalone"
+++

Standalone installation might be useful for environments where Docker is not suitable.

## Install signal-cli

Either install *signal-cli* using [packaging/signal-cli](https://packaging.gitlab.io/signal-cli) or from upstream [signal-cli Releases](https://github.com/AsamK/signal-cli).

{{< tabs groupId="installation" >}}
{{% tab name="Install from packages" %}}
See [Documentation](https://packaging.gitlab.io/signal-cli). Make sure to add `--tcp` to `/etc/default/signal-cli`.
{{% /tab %}}
{{% tab name="Install from signal-cli Releases" %}}
* create a dedicated user: `useradd -r -s /usr/sbin/nologin -m -d /var/lib/signal-cli signal-cli`
* follow [signal-cli installation instructions](https://github.com/AsamK/signal-cli#installation)
  * for all actions using the cli, please run as signal-cli user (`sudo -u signal-cli -H signal-cli ...`)
* setup the jsonrpc mode:
  * make sure `ExecStart` in `signal-cli.service` looks like this:
    * `ExecStart=%your-installation-dir%/bin/signal-cli -a %your_phonenumber% daemon --[tcp [PORT]|--socket [SOCKET]]`
{{% /tab %}}
{{< /tabs >}}


## Install python-signal-cli-rest-api

When *signal-cli* has been installed and setup properly, you can install the gateway:

### System packages

* `apt-get install python3-virtualenv`

### Create virtualenv

* `sudo -u signal-cli -i bash -c 'python3 -m virtualenv -p /usr/bin/python3 "${HOME}"/venv'`

### Install python-signal-cli-rest-api

* `sudo -u signal-cli -i bash -c '"${HOME}"/venv/bin/pip install python-signal-cli-rest-api'`

### Install SystemD units

{{< tabs groupId="systemd" >}}
{{% tab name="signal-cli daemon w/o USERNAME parameter" %}}
* copy [contrib/systemd/python-signal-cli-rest-api@.service](https://gitlab.com/morph027/python-signal-cli-rest-api/-/raw/main/contrib/systemd/python-signal-cli-rest-api@.service) into `/etc/systemd/system/`
* enable service

```bash
curl -L "https://gitlab.com/morph027/python-signal-cli-rest-api/-/raw/main/contrib/systemd/python-signal-cli-rest-api@.service" > "/etc/systemd/system/python-signal-cli-rest-api@.service"
systemctl enable --now "python-signal-cli-rest-api@$(systemd-escape <your_phonenumber>).service"
```

{{% /tab %}}
{{% tab name="signal-cli daemon w/ USERNAME parameter" %}}
* copy [contrib/systemd/python-signal-cli-rest-api.service](https://gitlab.com/morph027/python-signal-cli-rest-api/-/raw/main/contrib/systemd/python-signal-cli-rest-api.service) into `/etc/systemd/system/`
* enable service

```bash
curl -L "https://gitlab.com/morph027/python-signal-cli-rest-api/-/raw/main/contrib/systemd/python-signal-cli-rest-api.service" > /etc/systemd/system/python-signal-cli-rest-api.service
systemctl enable --now python-signal-cli-rest-api.service
```
{{% /tab %}}
{{< /tabs >}}

## Configure python-signal-cli-rest-api

Configuration can be done in `/etc/default/python-signal-cli-rest-api`. For possible values, see [Configuration]({{< ref "/configuration" >}} "Configuration")

## Install reverse proxy

It is advised to setup a reverse proxy in front of the gateway to add TLS, authentication and so on.
As there are plenty of proxies or webservers: nginx, apache, caddy, traefik,...), just pick your favourite.
