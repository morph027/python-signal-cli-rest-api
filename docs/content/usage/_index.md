+++
title = "Usage"
weight = 3
+++

These are some examples. For more info, check the [API documentation](python-signal-cli-rest-api/redoc/).

### Registration

[How to get CAPTCHA!](https://github.com/AsamK/signal-cli/wiki/Registration-with-captcha)

```
curl -d '{"use_voice": true, "captcha": "'${CAPTCHA}'"}' http://localhost:8080/v1/register/${account}
curl -X POST http://localhost:8080/v1/register/${account}/verify/${token}
```

### Plain text

```
curl -d '{"message": "foo"}' http://localhost:8080/v1/send/${phonenumber}
curl -d '{"recipients": ["${phonenumber}"], "message": "foo", "number": "${account}"}' http://localhost:8080/v2/send
```

### With attachments

Attachments needs to be bas64 encoded (e.g. `base64 -w 0 < file`).

```
curl -d '{"message": "bar with image", "base64_attachments": ["base64-encoded-string"]}' http://localhost:8080/v1/send/${phonenumber}
curl -d '{"recipients": ["${phonenumber}"], "message": "bar with image", "number": "${account}", "base64_attachments": ["base64-encoded-string"]}' http://localhost:8080/v2/send
```

### Send to groups

You can find the group id via an [API call](python-signal-cli-rest-api/swagger/index.html#/Groups/get~groups_of_number_v1.groups_for_number_get") or from the files in signal-cli data directory (`~/.local/share/signal-cli/data/<account-id>`)

{{% notice info %}}
**You need to replace `/`, `+` and `=` with `-` (dash) for v1**
{{% /notice %}}

```
curl -d '{"message": "foobar to all"}' http://localhost:8080/v1/send/${groupid.replace("/", "-")]
curl -d '{"recipients": ["${groupid}"], "message": "foobar to all", "number": "${account}"}' http://localhost:8080/v2/send
```

### Send with mentions

See [upstream issue](https://github.com/AsamK/signal-cli/issues/584#issuecomment-831434762):

> specifies start index and length for each mention `3:1:+NUMBER_MENTION`

This example will prepend (Index=0, Length of replacement=0) the whole message with a single mention:

```
curl -d '{"recipients": ["${groupid}"], "message": "foobar to all", "number": "${account}", "mentions": ["0:0:${phonenumber}"]}}' http://localhost:8080/v2/send
```
