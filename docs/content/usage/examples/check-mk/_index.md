+++
title = "Check_MK"
+++

## Configuration

Create a file `/omd/sites/<site-name>/local/share/check_mk/notifications/python-signal-cli-rest-api`

```python
#!/usr/bin/python3
# Signal using HTTP Gateway
# Notification via Signal using HTTP Gateway

import os, sys, requests

if os.environ.get('NOTIFY_WHAT') == 'SERVICE':
    if os.environ.get('NOTIFY_SERVICESTATE') == "OK":
        message = u"\U00002705"
    elif os.environ.get('NOTIFY_SERVICESTATE') == "WARNING":
        message = u"\U000026A0"
    elif os.environ.get('NOTIFY_SERVICESTATE') == "CRITICAL":
        message = u"\U0000274C"
    elif os.environ.get('NOTIFY_SERVICESTATE') == "UNKNOWN":
        message = u"\U00002734"
    else:
        message = ''
    message = f'{message} {os.environ.get("NOTIFY_HOSTNAME")} {os.environ.get("NOTIFY_SERVICESTATE")} {os.environ.get("NOTIFY_SERVICEDESC")} {os.environ.get("NOTIFY_SERVICEOUTPUT")}'
elif os.environ.get('NOTIFY_WHAT') == 'HOST':
    if os.environ.get('NOTIFY_HOSTSTATE') == "UP":
        message = u"\U00002705"
    elif os.environ.get('NOTIFY_HOSTSTATE') == "DOWN":
        message = u"\U0000274C"
    else:
        message = ""
    message = f'{message} {os.environ.get("NOTIFY_HOSTNAME")} is {os.environ.get("NOTIFY_HOSTSTATE")}'

recipient = os.environ.get('NOTIFY_CONTACTPAGER')

if not recipient:
    sys.stderr.write('Error: No contact pager set for %s\n' % os.environ.get('NOTIFY_CONTACTNAME'))
    sys.exit(1)

requests.post(f"{os.environ.get('NOTIFY_PARAMETER_1')}{recipient}", json={'message': message})
```

Now you can setup the notifications for your user or global. Create a new notification rule and select **Signal using HTTP Gateway** as *Notification Method*. Enter the gateway URL (including trailing slash) as first parameter. Finally setup a *Pager address* in your user account to receive messages.
