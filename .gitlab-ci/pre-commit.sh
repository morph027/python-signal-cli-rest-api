#!/bin/sh

set -e

apk --no-cache --quiet --no-progress add \
  py3-pip \
  build-base \
  python3-dev \
  py3-virtualenv \
  py3-magic \
  py3-yaml \
  git
virtualenv --system-site-packages "${VENV}"
export PATH="${VENV}"/bin:"${PATH}"
pip3 install pre-commit
git fetch origin
pre-commit run --from-ref origin/"${CI_DEFAULT_BRANCH}" --to-ref HEAD
