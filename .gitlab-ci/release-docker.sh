#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
  set -x
fi

image="${CI_REGISTRY}/${CI_PROJECT_NAMESPACE,,}/${CI_PROJECT_NAME}/${IMAGE_VARIANT}"

if [[ -n "${CI}" ]]; then
  if [[ -n "${CI_COMMIT_TAG}" ]]; then
     sed -i 's,^__version__ = .*,__version__ = "'"${CI_COMMIT_TAG}"'",' python_signal_cli_rest_api/version/__init__.py
  fi
fi

if ! command -v buildx >/dev/null 2>&1; then
  apk --no-cache add curl
  curl -Lo /usr/local/bin/buildx "https://github.com/docker/buildx/releases/download/v${BUILDX_VERSION}/buildx-v${BUILDX_VERSION}.linux-amd64"
  chmod +x /usr/local/bin/buildx
fi
if ! buildx inspect multiarch >/dev/null 2>&1; then
  buildx create --name multiarch
fi
buildx use multiarch
buildx inspect --bootstrap
buildx="buildx build"
if [[ -n "${CI}" ]]; then
  buildx+=" --output type=local,dest=/tmp/scan-me-please"
else
  buildx+=" --load"
fi
tags="-t ${image}:${CI_COMMIT_REF_SLUG}"

if [[ -n "${CI_COMMIT_TAG}" ]] || [[ "${CI_COMMIT_BRANCH}" == "main" ]]; then
  tags+=" -t ${image}:latest"
fi

# build and push/load
if docker pull "${image}":latest; then
  BUILDX_ARGS+=" --cache-from ${image}:latest"
fi
${buildx} --platform ${PLATFORMS} \
  ${BUILDX_ARGS} ${tags} \
  -f .docker/Dockerfile .

if ! command -v trivy >/dev/null 2>&1; then
  curl -Lo /tmp/trivy.tar.gz "https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz"
  tar -C /usr/local/bin -xzf /tmp/trivy.tar.gz trivy
fi
platform_to_scan="${PLATFORMS##*,}"
trivy --quiet rootfs --exit-code 10 --severity MEDIUM,HIGH,CRITICAL --no-progress --ignore-unfixed /tmp/scan-me-please/"${platform_to_scan/\//_}"
buildx build --push --platform ${PLATFORMS} \
  ${BUILDX_ARGS} ${tags} \
  -f .docker/Dockerfile .
