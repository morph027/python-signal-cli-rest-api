#!/bin/bash

set -e

cat > ~/.pypirc <<EOF
[pypi]
  username = __token__
  password = ${PYPI_TOKEN}
EOF

if [[ -n "${CI}" ]]; then
  sed -i 's,^__version__ = .*,__version__ = "'"${CI_COMMIT_TAG}"'",' python_signal_cli_rest_api/version/__init__.py
fi

apk --no-cache --quiet --no-progress add \
  py3-virtualenv
python3 -m virtualenv /tmp/venv
. /tmp/venv/bin/activate
pip3 install build twine
python3 -m build
twine upload dist/*"${CI_COMMIT_TAG//-}"*
