**UNMAINTAINED**

This project is not actively maintained anymore as signal-cli is now offering a native JSON-RPC api via HTTP.

# python-signal-cli-rest-api

REST Wrapper for [signal-cli](https://github.com/AsamK/signal-cli), replacement for [signal-web-gateway](https://gitlab.com/morph027/signal-web-gateway).

## Documentation

* [Documentation](https://morph027.gitlab.io/python-signal-cli-rest-api)

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).
